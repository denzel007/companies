<?php

namespace Tetrapak07\Companies;

use Phalcon\Mvc\Controller;
use Supermodule\ControllerBase as SupermoduleBase;

class CompaniesBase extends Controller
{
      
    public function initialize()
    {
        if (!SupermoduleBase::checkAndConnectModule('companies')) {
           header("Location: /error/show404");
           exit;
        } 
    }
    
    public function onConstruct()
    {
      $this->dataReturn = true;
        if (!SupermoduleBase::checkAndConnectModule('companies')) {
            $this->dataReturn = false;
        } else {
            $this->view->setViewsDir($this->config->modules->companies->viewsDir);
            $this->view->setTemplateBefore($this->config->modules->companies->templateBefore);
        }    
    }
}    
