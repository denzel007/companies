<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use Phalcon\Mvc\Model\Migration;

/**
 * Class CompaniesMigration_102
 */
class CompaniesMigration_102 extends Migration
{
    /**
     * Define the table structure
     *
     * @return void
     */
    public function morph()
    {
        $this->morphTable('companies', [
                'columns' => [
                    new Column(
                        'id',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'autoIncrement' => true,
                            'size' => 10,
                            'first' => true
                        ]
                    ),
                    new Column(
                        'name',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'id'
                        ]
                    ),
                    new Column(
                        'alias',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'notNull' => true,
                            'size' => 100,
                            'after' => 'name'
                        ]
                    ),
                    new Column(
                        'logo',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "",
                            'size' => 255,
                            'after' => 'alias'
                        ]
                    ),
                    new Column(
                        'emailSender',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 100,
                            'after' => 'logo'
                        ]
                    ),
                    new Column(
                        'image1',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "",
                            'size' => 255,
                            'after' => 'emailSender'
                        ]
                    ),
                    new Column(
                        'image2',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "",
                            'size' => 255,
                            'after' => 'image1'
                        ]
                    ),
                    new Column(
                        'image3',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'default' => "",
                            'size' => 255,
                            'after' => 'image2'
                        ]
                    ),
                    new Column(
                        'points',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'image3'
                        ]
                    ),
                    new Column(
                        'defaultLocaleId',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'unsigned' => true,
                            'notNull' => true,
                            'size' => 3,
                            'after' => 'points'
                        ]
                    ),
                    new Column(
                        'webShop',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "0",
                            'notNull' => true,
                            'size' => 1,
                            'after' => 'defaultLocaleId'
                        ]
                    ),
                    new Column(
                        'syncUrl',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "0",
                            'notNull' => true,
                            'size' => 1,
                            'after' => 'webShop'
                        ]
                    ),
                    new Column(
                        'receiptPhoto',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "0",
                            'notNull' => true,
                            'size' => 1,
                            'after' => 'syncUrl'
                        ]
                    ),
                    new Column(
                        'booking',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "0",
                            'notNull' => true,
                            'size' => 1,
                            'after' => 'receiptPhoto'
                        ]
                    ),
                    new Column(
                        'multipleLocations',
                        [
                            'type' => Column::TYPE_CHAR,
                            'default' => "0",
                            'notNull' => true,
                            'size' => 1,
                            'after' => 'booking'
                        ]
                    ),
                    new Column(
                        'postsSent',
                        [
                            'type' => Column::TYPE_INTEGER,
                            'default' => "0",
                            'notNull' => true,
                            'size' => 11,
                            'after' => 'multipleLocations'
                        ]
                    ),
                    new Column(
                        'appStore',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 2083,
                            'after' => 'postsSent'
                        ]
                    ),
                    new Column(
                        'googlePlay',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 2083,
                            'after' => 'appStore'
                        ]
                    ),
                    new Column(
                        'key1',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'googlePlay'
                        ]
                    ),
                    new Column(
                        'key2',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'key1'
                        ]
                    ),
                    new Column(
                        'key3',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 255,
                            'after' => 'key2'
                        ]
                    ),
                    new Column(
                        'pushPackageAndroid',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 50,
                            'after' => 'key3'
                        ]
                    ),
                    new Column(
                        'pushPackageIos',
                        [
                            'type' => Column::TYPE_VARCHAR,
                            'size' => 50,
                            'after' => 'pushPackageAndroid'
                        ]
                    )
                ],
                'indexes' => [
                    new Index('PRIMARY', ['id'], 'PRIMARY'),
                    new Index('defaultLocaleId', ['defaultLocaleId'], null)
                ],
                'references' => [
                    new Reference(
                        'companies_ibfk_1',
                        [
                            'referencedTable' => 'locale',
                            'columns' => ['defaultLocaleId'],
                            'referencedColumns' => ['id'],
                            'onUpdate' => 'RESTRICT',
                            'onDelete' => 'RESTRICT'
                        ]
                    )
                ],
                'options' => [
                    'TABLE_TYPE' => 'BASE TABLE',
                    'AUTO_INCREMENT' => '10',
                    'ENGINE' => 'InnoDB',
                    'TABLE_COLLATION' => 'utf8_general_ci'
                ],
            ]
        );
    }

    /**
     * Run the migrations
     *
     * @return void
     */
    public function up()
    {

    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {

    }

    /**
     * This method is called after the table was created
     *
     * @return void
     */
     public function afterCreateTable()
     {
        $this->batchInsert('companies', [
                'id',
                'name',
                'alias',
                'logo',
                'emailSender',
                'image1',
                'image2',
                'image3',
                'points',
                'defaultLocaleId',
                'webShop',
                'syncUrl',
                'receiptPhoto',
                'booking',
                'multipleLocations',
                'postsSent',
                'appStore',
                'googlePlay',
                'key1',
                'key2',
                'key3',
                'pushPackageAndroid',
                'pushPackageIos'
            ]
        );
     }
}
